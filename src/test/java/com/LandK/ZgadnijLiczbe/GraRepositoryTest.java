package com.LandK.ZgadnijLiczbe;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class GraRepositoryTest {

    @Autowired
    private  GraRepository graRepository;

    @Test
    void czyTylko10(){
        //given tworzymy 20 gier dodajemy im losową ilość ruchów oraz ustalamy wygraną i zapisujemy ich do bazy
        for (int i = 0; i < 20; i++){
            Date date = new Date();
            Gra gra = new Gra(date);
            Random r = new Random();
            gra.setLiczbaRuchow(r.nextInt(20)+2);
            gra.wygrana();
            this.graRepository.save(gra);
        }

        //when wywołujemy zapytanie do bazy o 10 najlepszych wyników
        List<Gra> listaGier = graRepository.getTop10();

        //then sprawdź czy querry zwróciło tylko 10 wyników
        assertThat(listaGier.size()).isEqualTo(10);
    }

    @Test
    void czySaWygrane(){
        //given tworzymy 20 gier dodajemy im losową ilość ruchów oraz losujemy czy wyrgrali i zapisujemy ich do bazy
        //w ten sposób w bazie będziemy mieli parę gier ukończonych i parę niedokończonych
        for (int i = 0; i < 20; i++){
            Date date = new Date();
            Gra gra = new Gra(date);
            Random r = new Random();
            gra.setLiczbaRuchow(r.nextInt(20)+2);
            if (r.nextInt(2)+1%2==0) gra.wygrana();
            this.graRepository.save(gra);
        }

        //when wywołujemy zapytanie do bazy o 10 najlepszych wyników
        List<Gra> listaGier = graRepository.getTop10();

        //then sprawdzamy czy wyszukane wyniki mają kolumnę wygrana na true
        Boolean czyWszystkieMajaWygrana = true;
        for(Gra gra: listaGier){
            if (!gra.isWygrana()) czyWszystkieMajaWygrana = false;
        }
        assertThat(czyWszystkieMajaWygrana).isEqualTo(true);
    }
}