package com.LandK.ZgadnijLiczbe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class GraController {

    @Autowired
    private GraRepository graRepository;

    /***
     * Endpoint start
     * Tworzy  obiekt daty który podajemy do konstruktora klasy Gra
     * zapisujemy obiekt Gra do bazy danych poprzez repozytorium
     * @return zwraca nr sesji uworzonego obiektu Gra z bazy danych
     */
    @RequestMapping(path = "/start")
    public Long start(){
        Date date = new Date();
        System.out.println(date);
        Gra gra = new Gra(date);
        graRepository.save(gra);
        return graRepository.getById(gra.getSesja()).getSesja();
    }

    /***
     * Endpoint guess
     * pobiera dwa parametry z linku
     * - sesja (nr sesji potrzebne żeby wyciągnąć dane z bazy)
     * - liczbę (nr który zgaduje użytkownik)
     * pobieramy z bazy obiekt gry na podstawie nr sesji
     * jeżeli właściwość wygrana będzie miała wartość true zwracamy czas gry, liczbę ruchów i informacje o wygranej
     * jeżeli liczba przysłana równa się liczbie wygenerowanej na starcie właściwość wygrana zostanie zmieniona na true i wykona się funkcja "wygrana" informacje w bazie zostają zaktualizowane
     * jeżeli liczba przysłana będzie większa zostanie wykonana funcka wykonajRuch oraz wiadomość dla użykownika zostanie zmieniona na "Większa"
     * jeżeli liczba przysłana będzie mniejsza zostanie wykonana funcka wykonajRuch oraz wiadomość dla użykownika zostanie zmieniona na "Mniejsza"
     * @param sesja unikalny nr sesji
     * @param Liczba liczba przesłana przez użytkownika
     * @return zwracamy Jsona w którym zawarta jest ilość ruchów, nr sejsii i wiadomość
     */
    @GetMapping(path = "/guess/{sesja}")
    public Map<String, String> guess(@PathVariable("sesja") long sesja, int Liczba){
        HashMap<String, String> map = new HashMap<>();
        Gra gra = graRepository.getById(sesja);
        map.put("Sesja", gra.getSesja()+"");
        if(gra.isWygrana()){
            map.put("Liczba prób", gra.getLiczbaRuchow()+"");
            gra.setWiadomosc("Wygrana! Gratulacje");
            map.put("Wiadomość", gra.getWiadomosc()+"");
            graRepository.save(gra);
            return map;
        }
        if(Liczba == gra.getLiczba()){
            gra.wykonajRuch();
            map.put("Liczba prób", gra.getLiczbaRuchow()+"");
            gra.wygrana();
            gra.setWiadomosc("Wygrana! Gratulacje");
            gra.setWygrana(true);
            map.put("Wiadomość", gra.getWiadomosc()+"");
            graRepository.save(gra);
            return map;
        }else if(Liczba > gra.getLiczba()){
            gra.setWiadomosc("Mniejsza");
            gra.wykonajRuch();
            map.put("Liczba prób", gra.getLiczbaRuchow()+"");
            map.put("Wiadomość", gra.getWiadomosc()+"");
            graRepository.save(gra);
            return map;
        }else if(Liczba < gra.getLiczba()){
            gra.setWiadomosc("Większa");
            gra.wykonajRuch();
            map.put("Liczba prób", gra.getLiczbaRuchow()+"");
            map.put("Wiadomość", gra.getWiadomosc()+"");
            graRepository.save(gra);
            return map;
        }
        return map;
    }

    /***
     * Endpoint hiscores
     * Wywołuje z klasy repozytorium funkcję getTop10
     * na podstawie tego co zwróci baza mapujemy jsona
     * @return dla każdego wyniku z top 10 zwracamy czas, liczbę ruchów i nr sesji
     */
    @GetMapping(path = "/hiscores")
    public List<Map<String , String>> hiscores(){
        List<Map<String, String >> Lista = new ArrayList<>();
        List<Gra> listaGra = graRepository.getTop10();
        HashMap<String, String> map = new HashMap<>();
        for (Gra gra : listaGra) {
            map.put("Czas", gra.getCzas() +" sekund");
            map.put("Ilość Ruchów", gra.getLiczbaRuchow()+"");
            map.put("Sesja", gra.getSesja()+"");
            Lista.add(map);
            map = new HashMap<>();
        }
        return Lista;
    }

}
