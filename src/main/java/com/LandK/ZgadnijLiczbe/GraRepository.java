package com.LandK.ZgadnijLiczbe;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GraRepository extends JpaRepository<Gra, Long>{

    /***
     * Funkcja wykonuje zapytanie do bazy danych, szuka gier z wygraną na true oraz sortuje je najpierw po liczbie ruchów a później po czasie rozgrywki
     * @return zwraca listę 10 obiektów Gra
     */
    @Query(value = "SELECT * FROM Gra u where wygrana = true ORDER BY liczba_ruchow, czas LIMIT 10", nativeQuery = true)
    public List<Gra> getTop10();
}
