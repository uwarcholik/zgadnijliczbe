package com.LandK.ZgadnijLiczbe;

import javax.persistence.*;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Entity
@Table
public class Gra implements java.io.Serializable{
    @Id
    @SequenceGenerator(name= "gra_sequence", sequenceName = "gra_sequence", allocationSize = 1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "gra_sequence")
    private long sesja;
    private int liczba;
    private int liczbaRuchow;
    private boolean wygrana;
    @Transient
    private String wiadomosc;
    private Date startCzas;
    private Date stopCzas;
    private Long czas;

    /***
     * Pusty konstruktor
     */
    public Gra() {
    }

    /***
     * Funkcja zbiera wszystkie parametry klasy i wrzuca je do jednego stringa
     * @return
     */
    @Override
    public String toString() {
        return "Gra{" +
                "sesja=" + sesja +
                ", liczba=" + liczba +
                ", liczbaRuchow=" + liczbaRuchow +
                ", wygrana=" + wygrana +
                ", wiadomosc='" + wiadomosc + '\'' +
                ", startCzas=" + startCzas +
                ", stopCzas=" + stopCzas +
                ", czas=" + czas +
                '}';
    }

    /***
     * konstruktor klasy używany w endpoint 'start' by utworzyć nowego gracza gotowego do rozgrywki
     * Odbywa się w nim losowanie liczby do zgadnięcia w przedziale od 0 do 100
     * Zerowana jest liczba ruchów
     * do startowego czasu zostaje przypisana data rozpoczęcia gry
     * wygrana ustawiana jest na false
     * @param startCzas - przez ten parametr dostarczana jest godzina startu gry
     */
    public Gra(Date startCzas) {
        Random r = new Random();
        this.liczba = r.nextInt(100) + 0;
        this.liczbaRuchow = 0;
        this.startCzas = startCzas;
        this.wygrana = false;
    }

    /***
     * Funkcja zwiększa ilość ruchów gracza
     */
    public void wykonajRuch(){
        this.liczbaRuchow++;
    }

    /***
     * Funcka wykonuje wszystkie czynności potrzebne do zarejestrowania wygranej
     * wygrana ustawiana na true
     * zostaje pobrana aktualna data by zapisać czas zakończenia rozgrywki
     * zostaje przekalkulowany czas rozgrywki na podstawie czasu rozpoczęcia i zakończenia
     */
    public void wygrana() {
        this.wygrana = true;
        Date date = new Date();
        this.stopCzas = date;
        this.czas = TimeUnit.SECONDS.convert(this.stopCzas.getTime() - this.startCzas.getTime(),TimeUnit.MILLISECONDS);
    }

    public long getSesja() {
        return this.sesja;
    }

    public int getLiczba() {
        return liczba;
    }

    public int getLiczbaRuchow() {
        return liczbaRuchow;
    }

    public boolean isWygrana() {
        return wygrana;
    }

    public String getWiadomosc() {
        return wiadomosc;
    }

    public Date getStartCzas() {
        return startCzas;
    }

    public Date getStopCzas() {
        return stopCzas;
    }

    public Long getCzas() {
        return czas;
    }

    public void setSesja(long sesja) {
        this.sesja = sesja;
    }

    public void setLiczba(int liczba) {
        this.liczba = liczba;
    }

    public void setLiczbaRuchow(int liczbaRuchow) {
        this.liczbaRuchow = liczbaRuchow;
    }

    public void setWygrana(boolean wygrana) {
        this.wygrana = wygrana;
    }

    public void setWiadomosc(String wiadomosc) {
        this.wiadomosc = wiadomosc;
    }

    public void setStartCzas(Date startCzas) {
        this.startCzas = startCzas;
    }

    public void setStopCzas(Date stopCzas) {
        this.stopCzas = stopCzas;
    }

    public void setCzas(Long czas) {
        this.czas = czas;
    }

}
