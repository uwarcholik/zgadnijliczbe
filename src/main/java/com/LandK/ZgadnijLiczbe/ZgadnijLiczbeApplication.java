package com.LandK.ZgadnijLiczbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZgadnijLiczbeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZgadnijLiczbeApplication.class, args);
	}
}
